package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

public class BasePage {

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    protected static WebDriver driver;

    public static void OpenUrl(String url){
        System.setProperty("Java.rmi.server.hostname","10.172.68.173");
        String Os = System.getProperty("os.name").toLowerCase();
        if (Os.equals("linux")){
            System.setProperty("webdriver.chrome.driver","chromedriver87Linux.exe");
        } else if(Os.equals("windows")){
            System.setProperty("webdriver.chrome.driver","chromedriver87Windows.exe");
        }
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
    }

    static Map<String,String> mapa = new HashMap<String,String>();

    static Map<String,Map> mapaPage = new HashMap<String, Map>();

    public static Map<String,Map> getPage() {
        mapaPage.put("Enter Vehicle Data", (new EnterVehicleData(driver).getMapa()));
        mapaPage.put("Enter Insurant Data", (new EnterInsurantData(driver).getMapa()));
        mapaPage.put("Enter Product Data", (new EnterProductData(driver).getMapa()));
        mapaPage.put("Select Price Option", (new SelectPriceOption(driver).getMapa()));
        mapaPage.put("Send Quote", (new SendQuote(driver).getMapa()));
        return mapaPage;
    }

    public static void setPage(String pagina){
        mapa = getPage().get(pagina);
    }
    
    public static void click(String field) {
        elementToBeClickable(getBy(field));
        driver.findElement(By.xpath(getBy(field))).click();
    }

    public static void setText(String field, String Text) {
        elementExists(getBy(field));
        driver.findElement(By.xpath(getBy(field))).sendKeys(Text);
    }
    
    public static void selectOption(String field, String option){
        elementExists(getBy(field));
    	Select dropdown = new Select(driver.findElement(By.xpath(getBy(field))));
        dropdown.selectByVisibleText(option);
    }

    public static void Closebrowser(){
        driver.quit();
    }
    
	public static String getBy(String name) {
		return mapa.get(name);
	}

	public static void elementToBeClickable(String field){
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(field)));    }

    public static void elementExists(String field){
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(field)));    }

        public static void validaenvio(String msgValidation){
            elementExists(getBy("elementValidator"));
            Assert.assertTrue(driver.findElement(By.xpath(getBy("elementValidator"))).getText().equals(msgValidation));
            Closebrowser();
        }
}
