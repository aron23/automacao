package Pages;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;


public class EnterVehicleData extends BasePage {

    Map<String,String> mapa = new HashMap<String,String>();

    public EnterVehicleData(WebDriver driver) {
    	 super(driver);
    }
    
    public Map<String,String> getMapa() {
    	
    	mapa.put( "Make", new String( "//select[@id='make']" ));
    	mapa.put( "Model", new String( "//select[@id='model']" ));
        mapa.put( "Cylinder Capacity", new String("//input[@id='cylindercapacity']"));
        mapa.put( "Engine Performance", new String("//input[@id='engineperformance']"));
        mapa.put( "Date of Manufacture", new String("//input[@id='dateofmanufacture']"));
        mapa.put( "Number of Seats", new String("//select[@id='numberofseats']"));
        mapa.put( "Right Hand Drive No", new String("//input[contains(@id,'righthanddrive') and @value='No']/../span"));
        mapa.put( "Right Hand Drive Yes", new String("//input[contains(@id,'righthanddrive') and @value='Yes']/../span"));
        mapa.put( "Number of Seats Motorcycle", new String("//select[@id='numberofseatsmotorcycle']"));
        mapa.put( "Fuel Type", new String("//select[@id='fuel']"));
        mapa.put( "Payload", new String("//input[@id='payload']"));
        mapa.put( "Total Weight", new String("//input[@id='totalweight']"));
        mapa.put( "List Price", new String("//input[@id='listprice']"));
        mapa.put( "License Plate Number", new String("//input[@id='licenseplatenumber']"));
        mapa.put( "Annual Mileage", new String("//input[@id='annualmileage']"));
        mapa.put( "Next", new String("//button[@id='nextenterinsurantdata']"));

        return mapa;
    }
}