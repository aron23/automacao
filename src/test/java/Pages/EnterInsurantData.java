package Pages;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;


public class EnterInsurantData extends BasePage {

    Map<String,String> mapa = new HashMap<String,String>();

    public EnterInsurantData(WebDriver driver) {
    	 super(driver);
    }
    
    public Map<String,String> getMapa() {
    	
    	mapa.put( "First Name", new String( "//input[@id='firstname']" ));
    	mapa.put( "Last Name", new String( "//input[@id='lastname']" ));
        mapa.put( "Date of Birth", new String("//input[@id='birthdate']"));
        mapa.put( "Gender Male", new String("//input[@id='gendermale']/../span"));
        mapa.put( "Gender Female", new String("//input[@id='genderfemale']/../span"));
        mapa.put( "Street Adress", new String("//input[@id='streetaddress']"));
        mapa.put( "Country", new String("//select[@id='country']"));
        mapa.put( "Zip Code", new String("//input[@id='zipcode']"));
        mapa.put( "City", new String("//input[@id='city']"));
        mapa.put( "occupation", new String("//select[@id='occupation']"));
        mapa.put( "Hobbies Speeding", new String("//input[@id='speeding']/../span"));
        mapa.put( "Hobbies Bungee Jumping", new String("//input[@id='bungeejumping']/../span"));
        mapa.put( "Cliff Diving", new String("//input[@id='cliffdiving']/../span"));
        mapa.put( "Skydiving", new String("//input[@id='skydiving']/../span"));
        mapa.put( "Other", new String("//input[@id='other']/../span"));
        mapa.put( "Website", new String("//input[@id='website']"));
        mapa.put( "Picture", new String("//input[@id='picture']"));
        mapa.put( "Prev", new String("//button[@id='preventervehicledata']"));
        mapa.put( "Next", new String("//button[@id='nextenterproductdata']"));

        return mapa;
    }
}