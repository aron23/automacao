package Pages;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public class EnterProductData extends BasePage {

    Map<String,String> mapa = new HashMap<String,String>();

    public EnterProductData(WebDriver driver) {
    	 super(driver);
    }
    
    public Map<String,String> getMapa() {
    	
    	mapa.put( "Start Date", new String( "//input[@id='startdate']" ));
    	mapa.put( "Insurance Sum", new String( "//select[@id='insurancesum']" ));
        mapa.put( "Merit Rating", new String("//select[@id='meritrating']"));
        mapa.put( "Damage Insurance", new String("//select[@id='damageinsurance']"));
        mapa.put( "Optional Products Euro Protection", new String("//input[@id='EuroProtection']/../span"));
        mapa.put( "Optional Products Legal Defense Insurance", new String("//input[@id='EuroProtection']/../span"));
        mapa.put( "Courtesy Car", new String("//select[@id='courtesycar']"));
        mapa.put( "Prev", new String("//button[@id='preventerinsurancedata']"));
        mapa.put( "Next", new String("//button[@id='nextselectpriceoption']"));

        return mapa;
    }
}