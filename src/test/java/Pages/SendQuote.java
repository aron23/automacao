package Pages;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public class SendQuote extends BasePage {

    Map<String,String> mapa = new HashMap<String,String>();

    public SendQuote(WebDriver driver) {
    	 super(driver);
    }
    
    public Map<String,String> getMapa() {
    	
    	mapa.put( "E-Mail", new String( "//input[@id='email']" ));
        mapa.put( "Phone", new String( "//input[@id='phone']" ));
        mapa.put( "Username", new String( "//input[@id='username']" ));
        mapa.put( "Password", new String( "//input[@id='password']" ));
        mapa.put( "Confirm Password", new String("//input[@id='confirmpassword']"));
        mapa.put( "Comments", new String("//textarea[@id='Comments']"));
        mapa.put( "Next", new String("//button[@id='sendemail']"));
        mapa.put( "Prev", new String("//button[@id='prevselectpriceoption']"));
        mapa.put( "Main page", new String("//a[@id='backmain']"));
        mapa.put( "New Automobile Insurance", new String("//a[@id='newautomobileinsurance']"));
        mapa.put( "New Truck Insurance", new String("//a[@id='newtruckinsurance']"));
        mapa.put( "New Motorcycle Insurance", new String("//a[@id='newmotorcycleinsurance']"));
        mapa.put( "New Camper Insurance", new String("//a[@id='newcamperinsurance']"));
        mapa.put( "elementValidator", new String("//div[@class='sweet-alert showSweetAlert visible']/h2"));

        return mapa;
    }

}