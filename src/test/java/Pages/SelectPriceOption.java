package Pages;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public class SelectPriceOption extends BasePage {

    Map<String,String> mapa = new HashMap<String,String>();

    public SelectPriceOption(WebDriver driver) {
    	 super(driver);
    }
    
    public Map<String,String> getMapa() {
    	
    	mapa.put( "Select Option Silver", new String( "//input[@id='selectsilver']/../span" ));
        mapa.put( "Select Option Gold", new String( "//input[@id='selectgold']/../span" ));
        mapa.put( "Select Option Platinum", new String( "//input[@id='selectplatinum']/../span" ));
        mapa.put( "Select Option Ultimate", new String( "//input[@id='selectultimate']/../span" ));
        mapa.put( "View Quote", new String("//a[@id='viewquote']"));
        mapa.put( "Download Quote", new String("//a[@id='downloadquote']"));
        mapa.put( "Next", new String("//button[@id='nextsendquote']"));
        mapa.put( "Prev", new String("//button[@id='preventerproductdata']"));

        return mapa;
    }
}