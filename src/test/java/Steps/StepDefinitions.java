package Steps;

import Pages.BasePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitions {

    @Given("^Acesso a url (.*)$")
    public void acesso_a_url_hml_backoffice_hydrapdv_com_br_login_auth(String url) throws Throwable {
        BasePage.OpenUrl(url);
    }
    @When("^preencho o campo (.*) com (.*)$")
    public void preencho_o_campo_loja(String field, String text) {
        BasePage.setText(field,text);
    }

    @When("^seleciono o campo (.*) com (.*)$")
    public void preencho_o_campo(String field, String option) {
        BasePage.selectOption(field,option);
    }
        
    @When("^verifico se a tela (.*) está sendo exibida$")
    public void verifico_se_a_tela_esta_exibindo(String tela) {
    	  BasePage.setPage(tela);
    }

    @When("^(Aciono o botão|escolho a opção) (.*)$")
    public void clico_em_enviar(String acao, String field) {
        BasePage.click(field);
    }

    @Then("^a aplicação apresenta a mensagem (.*)$")
    public void validr_msg(String msg) {
        BasePage.validaenvio(msg);
    }
}