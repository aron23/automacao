package Steps;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Features" , plugin = {"html:target/cucumber-html-report"} , monochrome = true)


public class CucumberRunner {

}