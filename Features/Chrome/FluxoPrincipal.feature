# language: pt
Funcionalidade: Enter Vehicle Data

  Cenário: Preencher formúlario Enter Vehicle Data
    Dado Acesso a url http://sampleapp.tricentis.com/101/app.php
    E verifico se a tela Enter Vehicle Data está sendo exibida
    Quando seleciono o campo Make com Audi
    E seleciono o campo Model com Scooter
    E preencho o campo Cylinder Capacity com 1500
    E preencho o campo Engine Performance com 500
    E preencho o campo Date of Manufacture com 10/05/2010
    E seleciono o campo Number of Seats com 2
    E escolho a opção Right Hand Drive Yes
    E seleciono o campo Number of Seats Motorcycle com 2
    E seleciono o campo Fuel Type com Petrol
    E preencho o campo Payload com 700
    E preencho o campo Total Weight com 900
    E preencho o campo List Price com 30000
    E preencho o campo License Plate Number com DDDDS
    E preencho o campo Annual Mileage com 6000
    E Aciono o botão Next

  Cenário: Preencher formulário Enter Insurant Data
    Dado verifico se a tela Enter Insurant Data está sendo exibida
    Quando preencho o campo First Name com Aron
    E preencho o campo Last Name com Nascimento
    E preencho o campo Date of Birth com 10/12/1989
    E escolho a opção Gender Male
    E preencho o campo Street Adress com Rua iluminada 330
    E seleciono o campo Country com Brazil
    E preencho o campo Zip Code com 21800449
    E preencho o campo City com Rio de Janeiro
    E seleciono o campo occupation com Employee
    E escolho a opção Hobbies Speeding
    E preencho o campo Website com www.loremlorem.com
    E preencho o campo Picture com automacaoNaSuaCasa/Captura de tela de 2021-01-09 11-59-21.png
    E Aciono o botão Next


  Cenário: Preencher formulário Enter Product Data
    Dado verifico se a tela Enter Product Data está sendo exibida
    Quando preencho o campo Start Date com 10/01/2021
    E seleciono o campo Insurance Sum com 3.000.000,00
    E seleciono o campo Merit Rating com Bonus 1
    E seleciono o campo Damage Insurance com Full Coverage
    E escolho a opção Optional Products Legal Defense Insurance
    E seleciono o campo Courtesy Car com Yes
    E Aciono o botão Next

  Cenário: Preencher formulário Select Price Option
    Dado verifico se a tela Select Price Option está sendo exibida
    Quando escolho a opção Select Option Ultimate
    E Aciono o botão Next

  Cenário: Preencher formulário Send Quote
    Dado verifico se a tela Send Quote está sendo exibida
    Quando preencho o campo E-Mail com lorem@lorem.com
    E preencho o campo Phone com 21999999999
    E preencho o campo Username com lorem.lorem
    E preencho o campo Password com Lorem123
    E preencho o campo Confirm Password com Lorem123
    E preencho o campo Comments com Fim do preenchimento dos formulários
    E Aciono o botão Next
    Então a aplicação apresenta a mensagem Sending e-mail success!

